mod cli;
mod coordinate;
mod graphics;
mod support;
mod shader;
mod workspace;

use graphics::{Item, show_items};
use workspace::Workspace;

fn main() {
    let direction = match cli::read_args() {
        Ok(args) => args.direction().expect("inconsistent direction"),
        Err(e) => e.exit(),
    };
    println!("{:?}", direction);
    let workspaces = workspace::get_workspaces()
        .expect("could not get workspaces");

    let target = match workspaces.iter().find(|x| x.focused) {
        Some(focused_workspace) =>
            coordinate::move_by(focused_workspace.coordinate, direction),
        None =>
            [0, 0],
    };
    workspace::move_to_workspace(target)
        .expect("workspace change failed");
    let items: Vec<Item> = workspaces.iter()
        .map(
            |Workspace { coordinate, .. }|
            Item { position: [
                (coordinate[0] - target[0]) as f32 * 0.2,
                (coordinate[1] - target[1]) as f32 * 0.2,
            ] }
        )
        .collect();

    show_items(items);
}
