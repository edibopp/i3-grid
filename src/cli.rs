use crate::coordinate::Direction;
use docopt::Docopt;
use failure::Error;
use serde::Deserialize;

const USAGE: &'static str = "
i3grid

Usage:
  i3grid move (left|right|up|down)
  i3grid (-h | --help)

Options:
  -h --help     Show this screen.
";

#[derive(Copy, Clone, PartialEq, Eq, Debug, Deserialize)]
pub struct Args {
    cmd_left: bool,
    cmd_right: bool,
    cmd_up: bool,
    cmd_down: bool,
}

impl Args {
  pub fn direction(&self) -> Result<Direction, Error> {
    if self.cmd_left {
      Ok(Direction::Left)
    } else if self.cmd_right {
      Ok(Direction::Right)
    } else if self.cmd_up {
      Ok(Direction::Up)
    } else if self.cmd_down {
      Ok(Direction::Down)
    } else {
      Err(failure::err_msg("inconsistent direction"))
    }
  }
}

pub fn read_args() -> Result<Args, docopt::Error> {
  let docopt = Docopt::new(USAGE)?;
  let args = docopt.deserialize()?;
  Ok(args)
}
